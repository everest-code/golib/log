package log

import (
	"os"
	"regexp"
	"testing"

	"github.com/fatih/color"
)

func TestPid(t *testing.T) {
	if pid() != os.Getpid() {
		t.Errorf("Expected: %v Got:%v", os.Getpid(), pid())
	}
}

func TestHostname(t *testing.T) {
	host, _ := os.Hostname()
	if hostname() != host && hostname() != "<nil>" {
		t.Errorf("Expected: %v Got: %v", host, hostname())
	}
}

func TestNowString(t *testing.T) {
	re := regexp.MustCompile("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$")
	date := nowString()
	if !re.MatchString(date) {
		t.Errorf("Format got is not valid, Value: %v", date)
	}
}

func TestCreatePrefix(t *testing.T) {
	re := regexp.MustCompile("^\\[INFO \\| \\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\]\\(.+ \\| \\d+\\)$")
	prefix := createPrefix("INFO")
	if !re.MatchString(prefix) {
		t.Errorf("Format got is not valid, Value: %v", prefix)
	}
}

func TestColorExpected(t *testing.T) {
	txt, col := "test", color.FgCyan
	if color.New(col).Sprint(txt) != colorize(col, txt, false) {
		t.Error("Color text expected is not got")
	}
}
