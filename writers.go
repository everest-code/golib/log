package log

import (
	"io"
	"sync"
)

type (
	ParallelWriter struct {
		Writers []io.Writer
	}

	StringWriter struct {
		Buffer string
		Size   int
	}
)

func (writer *ParallelWriter) Write(p []byte) (n int, err error) {
	defer (func() {
		if r := recover(); r != nil {
			err = r.(error)
		}
	})()

	var wg sync.WaitGroup
	n = len(p)

	for _, writer := range writer.Writers {
		wg.Add(1)
		go (func(wg *sync.WaitGroup, writer io.Writer, p []byte) {
			defer wg.Done()

			_, err = writer.Write(p)
			if err != nil {
				panic(err)
			}
		})(&wg, writer, p)
	}

	wg.Wait()
	return
}

func (writer *StringWriter) Write(p []byte) (n int, err error) {
	n = len(p)
	writer.Buffer += string(p)
	writer.Size += n

	return
}
