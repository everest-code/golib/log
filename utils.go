package log

import (
	"fmt"
	"os"
	"time"

	"github.com/fatih/color"
)

func nowString() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

func pid() int {
	return os.Getpid()
}

func hostname() string {
	host, err := os.Hostname()
	if err != nil {
		return "<nil>"
	}

	return host
}

func colorize(col color.Attribute, txt string, noColor bool) string {
	if noColor {
		return txt
	}

	return color.New(col).Sprint(txt)
}

func createPrefix(level string) string {
	return fmt.Sprintf("[%s | %s](%s | %d)", level, nowString(), hostname(), pid())
}