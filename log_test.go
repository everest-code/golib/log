package log

import "testing"

func TestLevelErro(t *testing.T) {
	w := &StringWriter{"", 0}
	log, _ := NewIO(w, LevelErro, true)

	log.Warn("hola")
	log.Info("hola")
	log.Debug("hola")

	if w.Buffer != "" {
		t.Error("Log raised and not in Level")
	}
}

func TestLevelWarn(t *testing.T) {
	w := &StringWriter{"", 0}
	log, _ := NewIO(w, LevelWarn, true)

	log.Info("hola")
	log.Debug("hola")

	if w.Buffer != "" {
		t.Error("Log raised and not in Level")
	}
}

func TestLevelInfo(t *testing.T) {
	w := &StringWriter{"", 0}
	log, _ := NewIO(w, LevelInfo, true)

	log.Debug("hola")

	if w.Buffer != "" {
		t.Error("Log raised and not in Level")
	}
}

func TestLevelDebug(t *testing.T) {
	w := &StringWriter{"", 0}
	log, _ := NewIO(w, LevelDebg, true)

	log.Debug("hola")

	if w.Buffer == "" {
		t.Error("Log raised and is in Level")
	}
}

func TestPanicRaise(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Error("Panic not raise error")
		}
	}()

	Panic("test")
}
