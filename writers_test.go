package log

import (
	"io"
	"testing"
)

func TestStringWriter(t *testing.T) {
	w := &StringWriter{"", 0}
	data := "hola"

	w.Write([]byte(data))

	if w.Buffer != data {
		t.Errorf("Expected buffer: %v, got: %v", data, w.Buffer)
	}

	if w.Size != len(data) {
		t.Errorf("Expected size: %v, got: %v", data, w.Size)
	}
}

func TestParallelWrite(t *testing.T) {
	data := "hola"
	w := &ParallelWriter{
		Writers: []io.Writer{
			&StringWriter{"", 0},
			&StringWriter{"", 0},
		},
	}

	w.Write([]byte(data))

	for k, innerWriter := range w.Writers {
		innerWriterConv, ok := innerWriter.(*StringWriter)
		if !ok {
			t.Errorf("Expected StringWriter got %#v", innerWriter)
		}

		if innerWriterConv.Buffer != data {
			t.Errorf("Expected buffer on (%d): %v, got: %v", k, data, innerWriterConv.Buffer)
		}

		if innerWriterConv.Size != len(data) {
			t.Errorf("Expected size on (%d): %v, got: %v", k, data, innerWriterConv.Size)
		}
	}
}
