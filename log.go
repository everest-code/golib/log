package log

import (
	"errors"
	"fmt"
	"io"
	"os"

	"github.com/fatih/color"
)

type (
	Level uint8

	Logger struct {
		writer  io.Writer
		Level   Level
		NoColor bool
	}
)

const (
	LevelDebg Level = iota
	LevelInfo
	LevelWarn
	LevelErro
)

func NewConsole(level Level, noColor bool) (logger *Logger, err error) {
	logger = &Logger{
		writer:  os.Stdout,
		Level:   level,
		NoColor: noColor,
	}

	return
}

func NewFile(level Level, fileName string) (logger *Logger, err error) {
	var file *os.File
	var flags int

	flags = os.O_CREATE | os.O_APPEND | os.O_WRONLY
	file, err = os.OpenFile(fileName, flags, 0o644)

	if err == nil {
		logger = &Logger{
			writer:  file,
			Level:   level,
			NoColor: true,
		}
	}

	return
}

func NewConsoleFile(fileName string, level Level, noColor bool) (logger *Logger, err error) {
	logger1, err := NewConsole(level, noColor)
	if err != nil {
		return
	}

	logger2, err := NewFile(level, fileName)
	if err != nil {
		return
	}

	logger, err = NewIO(&ParallelWriter{
		Writers: []io.Writer{logger1, logger2},
	}, level, noColor)

	return
}

func NewIO(io io.Writer, level Level, noColor bool) (logger *Logger, err error) {
	logger = &Logger{
		writer:  io,
		Level:   level,
		NoColor: noColor,
	}

	return
}

func Panic(format string, data ...interface{}) {
	logger, _ := NewConsole(LevelDebg, false)
	logger.Panic(format, data...)
}


func (logger *Logger) Write(p []byte) (int, error) {
	return logger.writer.Write(p)
}

func (logger *Logger) writeString(data string) {
	logger.Write([]byte(data + "\r\n"))
}

func (logger *Logger) Debug(format string, data ...interface{}) {
	if logger.Level > LevelDebg {
		return
	}

	var prefix string = createPrefix(colorize(color.FgHiGreen, "DEBG", logger.NoColor))
	var txt string = fmt.Sprintf(format, data...)

	logger.writeString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) ImportantInfo(format string, data ...interface{}) {
	var prefix string = createPrefix(colorize(color.FgHiBlue, "INFO", logger.NoColor))
	var txt string = fmt.Sprintf(format, data...)

	logger.writeString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) Info(format string, data ...interface{}) {
	if logger.Level > LevelInfo {
		return
	}

	var prefix string = createPrefix(colorize(color.FgCyan, "INFO", logger.NoColor))
	var txt string = fmt.Sprintf(format, data...)

	logger.writeString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) Warn(format string, data ...interface{}) {
	if logger.Level > LevelWarn {
		return
	}

	var prefix string = createPrefix(colorize(color.FgYellow, "WARN", logger.NoColor))
	var txt string = fmt.Sprintf(format, data...)

	logger.writeString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) Error(format string, data ...interface{}) {
	if logger.Level > LevelErro {
		return
	}

	var prefix string = createPrefix(colorize(color.FgRed, "ERR!", logger.NoColor))
	var txt string = fmt.Sprintf(format, data...)

	logger.writeString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) Panic(format string, data ...interface{}) {
	var prefix string = createPrefix(colorize(color.FgRed, "PN!!", logger.NoColor))
	var txt string = fmt.Sprintf(format, data...)

	panic(errors.New(fmt.Sprintf("%s: %s", prefix, txt)))
}

